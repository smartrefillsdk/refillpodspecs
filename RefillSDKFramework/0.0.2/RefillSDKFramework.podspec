Pod::Spec.new do |s|
  s.name             = 'RefillSDKFramework'
  s.version          = '0.0.2'
  s.summary          = 'A short description of RefillSDKFramework.'
  s.description      = "This is the SDK for the Refill Platform from Smart Refill. This allows our partner companies to provide top up functionality using cards and swish to the end user. The SDK also contains features to load localized texts, images and product catalog for a more consistent user experience across apps/platforms"

  s.homepage         = 'https://www.smartrefill.se'
  s.license          = { :type => 'Propriety', :file => 'LICENSE' }
  s.author           = { 'Nouman Tariq' => 'nouman.tariq@smartrefill.se' }
  s.source           = { :git => 'https://gitlab.com/smartrefillsdk/ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '12.0'
  s.platform = :ios
  s.swift_version = "4.0"

  # https://stackoverflow.com/questions/63607158/xcode-12-building-for-ios-simulator-but-linking-in-object-file-built-for-ios
  s.pod_target_xcconfig = { 'ONLY_ACTIVE_ARCH' => 'YES' }

  s.vendored_frameworks = "RefillSDK.xcframework"
  # Use the following line to add 3rd party pods as dependencies if needed
  # s.dependency 'AFNetworking', '~> 2.3'
end
